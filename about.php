<!DOCTYPE HTML>
<html>
<?php
include("head.php");
include("nav.php");
include("breadcrumbs.php");
?>

<head>
	<title>Footwear </title>


</head>

<body>

	<div class="colorlib-loader"></div>

	<div id="page">
		<div class="colorlib-about">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-sm-6 mb-3">
						<div class="video colorlib-video" style="background-image: url(images/I3.jpeg);">
							<a href="NIKE.mp4" class="popup-vimeo"><i class="icon-play3"></i></a>
							<div class="overlay"></div>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="about-wrap">
							<h2>Footwear</h2>
							<p>This E-Commerce website for footwear speacilizes in selling shoes of major brands with complete satisfaction to all the buyers.</p>
							<p>The major brands specified above are Nike , Adidas , Skechers to name a few. The collection provided by our website is truly remarkable in terms of quality , value for money and durability.</p>
							<p>Our whole team hopes that every customer is satisfied with all the products he/she buys and comes back to us when they need a new pair of kicks!
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
	</div>
	<?php
	include("footer.php");
	include("footerlinks.php");
	?>
</body>

</html>