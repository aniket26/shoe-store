<!DOCTYPE HTML>
<html>
<?php
include("head.php");
include("nav.php");
?>

<head>
	<script type="text/javascript" src="addval.js"></script>
</head>
<script>
	$('#myModal').on('shown.bs.modal', function() {
		$('#myInput').trigger('focus')
	})
</script>

<body>

	<div class="colorlib-loader"></div>

	<div id="page">
		<div class="colorlib-product">
			<div class="container">
				<div class="row row-pb-lg">
					<div class="col-sm-10 offset-md-1">
						<div class="process-wrap">
							<div class="process text-center active">
								<p><span>01</span></p>
								<h3>Shopping Cart</h3>
							</div>
							<div class="process text-center active">
								<p><span>02</span></p>
								<h3>Checkout</h3>
							</div>
							<div class="process text-center">
								<p><span>03</span></p>
								<h3>Order Complete</h3>
							</div>
						</div>
					</div>
				</div>
				<form method="POST" action="place_order_address.php" class="colorlib-form" name="Add">
					<div class="row">
						<div class="col-lg-8">

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									</div>
								</div>
								<div class="row-sm-8">
									<?php
									require("utils/conn.php");
									$sql = "SELECT address from address where username = '" . $_SESSION["username"] . "'";
									$result = $conn->query($sql);
									if ($result->num_rows > 0) {
										while ($row = $result->fetch_assoc()) {
											echo '<div class="row">';
											echo '<input type="radio" name="gender" value="'.$row['address'].'">'.$row["address"];
											echo '</div>';
										}
									}
									?>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<br><br>
										<!-- Button trigger modal -->
										<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
											Enter New Address
										</button>
									</div>
								</div>
							</div>
						</div>	
						
									<div class="col-lg-4">
										<div class="row">
											<div class="col-md-12">
												<div class="cart-detail">
													<h2>Cart Total</h2>
													<ul>
														<li>
															<p><span>Subtotal:</span> <span><?php echo "Rs." . $_SESSION["sum"]; ?></span></p>
														</li>
														<li><span>Delivery</span><span>Rs.100</span></li>
														<li>
															<p><span><strong>Grand Total:</strong></span> <span><?php echo "Rs." . $_SESSION["gtotal"]; ?></span></p>
														</li>
													</ul>
												</div>
											</div>

											<div class="w-100"></div>
											<div class="col-md-12">
												<div class="cart-detail">
													<h2>Payment Method</h2>
													<div class="form-group">
														<div class="col-md-12">
															<div class="radio">
																<label><input type="radio" name="optradio"> Direct Bank Tranfer</label>
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<div class="radio">
																<label><input type="radio" name="optradio"> Check Payment</label>
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<div class="radio">
																<label><input type="radio" name="optradio"> Paypal</label>
															</div>
														</div>
													</div>
													<div class="form-group">
														<div class="col-md-12">
															<div class="checkbox">
																<label><input type="checkbox" value=""> I have read and accept the terms and conditions</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-12 text-center">
												<p><button class="btn btn-primary" type="Submit">Place an order</button></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>
	</div>
	</form>
	<form action="insert_address.php" method="POST" name="Valid1" class="colorlib-form">
		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<textarea rows="4" cols="58" placeholder="Enter Address" name="address" id="Address" required></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Add Address</button>
					</div>
				</div>
			</div>
		</div>
	</form>
	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
	</div>
	<?php
	include("footer.php");
	include("footerlinks.php");
	?>
</body>

</html>