<!DOCTYPE HTML>
<html>
<?php
include("head.php");
include("nav.php");
include("breadcrumbs.php");
?>

<head>
	<script src="valid.js"></script>
	<title>Footwear</title>
</head>

<body>

	<div class="colorlib-loader"></div>

	<div id="page">
		<div id="colorlib-contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-12">
						<h3>Contact Information</h3>
						<div class="row contact-info-wrap">
							<div class="col-md-3">
								<p><span><i class="icon-location"></i></span>102,Sundaram, 2nd floor Near Satyam Shopping Center <br>M.G. Road, Ghatkopar(e) Mumbai-400077</p>
							</div>
							<div class="col-md-3">
								<p><span><i class="icon-phone3"></i></span> <a href="#">+ 1235 2355 98</a></p>
							</div>
							<div class="col-md-3">
								<p><span><i class="icon-paperplane"></i></span> <a href="#">info@yoursite.com</a></p>
							</div>
							<div class="col-md-3">
								<p><span><i class="icon-globe"></i></span> <a href="index.php">Shoe - Commerce</a></p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="contact-wrap">
							<h3>Get In Touch</h3>
							<!-- <div class="row" id="errDiv" style="color:red"></div> -->
							<form name="Valid" method="POST" action="insert_contact.php">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="fname">First Name</label>
											<input type="text" name="fname" id="fname" class="form-control" placeholder="Your firstname">
											<div class="container">
												<div class="row container" id="errDiv" style="color:red"></div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="lname">Last Name</label>
											<input type="text" name="lname" id="lname" class="form-control" placeholder="Your lastname">
											<div class="container">
												<div class="row container" id="errDiv1" style="color:red"></div>
											</div>
										</div>
									</div>
									<!-- <div class="col-sm-12">
										<div class="form-group">
											<label for="subject">Username</label>
											<input type="text" name="username" id="username" class="form-control" placeholder="Your username">
											<div class="container">
												<div class="row container" id="errDiv2" style="color:red"></div>
											</div>
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="subject">Mobile</label>
											<input type="text" name="number" id="number" minlength="10" maxlength="10" onkeypress="return restrictAlphabets(event)" class="form-control" placeholder="Your number">
											<div class="container">
												<div class="row container" id="errDiv3" style="color:red"></div>
											</div>
										</div>
									</div> -->
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="email">Email</label>
											<input type="text" name="email" id="email" class="form-control" placeholder="Your email address">
											<div class="container">
												<div class="row container" id="errDiv4" style="color:red"></div>
											</div>
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="subject">Subject</label>
											<input type="text" name="subject" id="subject" class="form-control" placeholder="Your subject">
											<div class="container">
												<div class="row container" id="errDiv5" style="color:red"></div>
											</div>
										</div>
									</div>
									<!-- <div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="message">Address</label>
											<textarea name="address" id="address" cols="20" rows="5" class="form-control" placeholder="Enter your address"></textarea>
											<div class="container">
												<div class="row container" id="errDiv6" style="color:red"></div>
											</div>
										</div>
									</div> -->
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<label for="message">Message</label>
											<textarea name="message" id="message" cols="30" rows="10" class="form-control" placeholder="Say something about us"></textarea>
											<div class="container">
												<div class="row container" id="errDiv7" style="color:red"></div>
											</div>
										</div>
									</div>
									<div class="w-100"></div>
									<div class="col-sm-12">
										<div class="form-group">
											<input type="submit" value="Submit" class="btn btn-primary" onclick="valid(event)">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-6">
						<div id="map" class="colorlib-map"></div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="ion-ios-arrow-up"></i></a>
	</div>
	<?php
	include("footer.php");
	include("footerlinks.php");
	?>
</body>

</html>