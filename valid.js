function valid(e) {
    let fname = document.forms["Valid"]["fname"];
    let lname = document.forms["Valid"]["lname"];
    // let username = document.forms["Valid"]["username"];
    // let number = document.forms["Valid"]["number"];
    let email = document.forms["Valid"]["email"];
    let subject = document.forms["Valid"]["subject"];
    // let address = document.forms["Valid"]["address"];
    let message = document.forms["Valid"]["message"];
    let errFname="";
    let errLname = "";
    // let errUser = "";
    // let errNum = "";
    let errEm = "";
    let errSub = "";
    // let errAdd = "";
    let errMess = "";
    if (fname.value.trim() === "") {
        errFname = errFname + "Enter valid First Name";
    }
    if (lname.value.trim() === "") {
        errLname = errLname + "Enter valid Last Name";
    }
    // if (username.value.trim() === "") {
    //     errUser = errUser + "<br>Enter valid Username";
    // }
    // if (number.value == "") {
    //     errNum = errNum + "<br>Enter valid Mobile Number";
    // }
    if (email.value.indexOf(".", 0) < 0 && email.value.indexOf("@", 0) < 0 && email.value.trim() ==""){
        errEm=errEm + "<br>Enter valid Email";
    }
    if (subject.value == "") {
        errSub = errSub + "<br>Enter valid Subject";
    }
    // if (address.value == "") {
    //     errAdd = errAdd + "<br>Enter valid Address";
    // }
    if (message.value == "") {
        errMess = errMess + "<br>Enter valid Message";
    }
    if(errFname!==""){
        document.getElementById("errDiv").innerHTML = errFname;
        e.preventDefault(); 
    }
    if (errLname !== "") {
        document.getElementById("errDiv1").innerHTML = errLname;
        e.preventDefault();
    }
    // if (errUser !== "") {
    //     document.getElementById("errDiv2").innerHTML = errUser;
    //     e.preventDefault();
    // }
    // if (errNum !== "") {
    //     document.getElementById("errDiv3").innerHTML = errNum;
    //     e.preventDefault();
    // }
    if (errEm !== "") {
        document.getElementById("errDiv4").innerHTML = errEm;
        e.preventDefault();
    }
    if (errSub !== "") {
        document.getElementById("errDiv5").innerHTML = errSub;
        e.preventDefault();
    }
    // if (errAdd !== "") {
    //     document.getElementById("errDiv6").innerHTML = errAdd;
    //     e.preventDefault();
    // }
    if (errMess !== "") {
        document.getElementById("errDiv7").innerHTML = errMess;
        e.preventDefault();
    }
}
function restrictAlphabets(e) {
    var x = e.which || e.keycode;
    if ((x >= 48 && x <= 57) || x == 8 ||
        (x >= 35 && x <= 40) || x == 46)
        return true;
    else
        return false;
}